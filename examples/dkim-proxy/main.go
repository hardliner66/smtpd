// Command dkim-proxy implements a simple SMTP proxy that DKIM signs incoming e-mail and relays to another SMTP server for delivery
package main

import (
	"flag"

	"fmt"

	"encoding/json"

	"bitbucket.org/hardliner66/smtpd"
	//"github.com/eaigner/dkim"
)

var (
	welcomeMsg = flag.String("welcome", "DKIM-proxy ESMTP ready.", "Welcome message for SMTP session")
	inAddr     = flag.String("inaddr", "127.0.0.1:12121", "Address to listen for incoming SMTP on")
	outAddr    = flag.String("outaddr", "127.0.0.1:10025", "Address to deliver outgoing SMTP on")
	debug      = flag.Bool("debug", false, "Enable debug messages")
	//privKeyFile = flag.String("key", "", "Private key file.")
	//dkimS       = flag.String("s", "default", "DKIM selector")
	//dkimD       = flag.String("d", "", "DKIM domain")

	//dkimConf dkim.Conf
	//privKey  []byte
)

type Message struct {
	Envelope smtpd.Envelope
	Peer     smtpd.Peer
}

func handler(peer smtpd.Peer, env smtpd.Envelope) error {

	//d, err := dkim.New(dkimConf, privKey)
	//if err != nil {
	//	log.Printf("DKIM error: %v", err)
	//	return smtpd.Error{450, "Internal server error"}
	//}

	//// The dkim package expects \r\n newlines, so replace to that
	//data, err := d.Sign(bytes.Replace(env.Data, []byte("\n"), []byte("\r\n"), -1))
	//if err != nil {
	//	log.Printf("DKIM signing error: %v", err)
	//	return smtpd.Error{450, "Internal server error"}
	//}

	b, err := json.Marshal(Message{env, peer})

	if err == nil {
		fmt.Println(string(b))
	} else {
		fmt.Println(err)
	}

	//return smtp.SendMail(
	//	*outAddr,
	//	nil,
	//	env.Sender,
	//	env.Recipients,
	//	env.Data,
	//)

}

func auth(peer *smtpd.Peer, username, password string, tls bool, authMethod string) error {

	//if !tls {
	//	return smtpd.Error{502, "Cannot AUTH in plain text mode. Use STARTTLS."}
	//}

	switch authMethod {

	case "LOGIN":

	case "PLAIN":

	default:
		//fmt.Println(smtpd.Error{502, "Unknown authentication mechanism"}, authMethod)
		return smtpd.Error{502, "Unknown authentication mechanism"}
	}

	if username != "steve" || password != "steve" {
		return smtpd.Error{535, "Authentication failed. Restarting authentication process."}
	} else {
		return nil
	}

	//peer.Username = username
	//peer.Password = password

}

func main() {

	flag.Parse()

	//var err error

	//dkimConf, err = dkim.NewConf(*dkimD, *dkimS)
	//if err != nil {
	//	log.Fatalf("DKIM configuration error: %v", err)
	//}

	//privKey, err = ioutil.ReadFile(*privKeyFile)
	//if err != nil {
	//	log.Fatalf("Couldn't read private key: %v", err)
	//}

	//_, err = dkim.New(dkimConf, privKey)
	//if err != nil {
	//	log.Fatalf("DKIM error: %v", err)
	//}

	server := &smtpd.Server{
		WelcomeMessage: *welcomeMsg,
		Handler:        handler,
		Authenticator:  auth,
		Debug:          *debug,
	}

	server.ListenAndServe(*inAddr)

}
